<?php

namespace Drupal\bs_asset\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to display implementation status.
 *
 * @Block(
 *   id="asset_implementation_status",
 *   admin_label = @Translation("Enterprise Asset Management implementation status")
 * )
 */
class ImplementationStatus extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => $this->t('Please check the <a href=":url">Enterprise Asset Management Implementation Status</a> and finish the system implementation.', [
        ':url' => Url::fromRoute('asset.implementation_status')->toString(),
      ])
    ];
  }

}
