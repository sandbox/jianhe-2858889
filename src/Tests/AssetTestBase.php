<?php

namespace Drupal\bs_asset\Tests;

use Drupal\bs_asset\Entity\Component;
use Drupal\cbo_item\Tests\ItemTestBase;
use Drupal\bs_asset\Entity\Asset;

/**
 * Provides tool functions.
 */
abstract class AssetTestBase extends ItemTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = array('bs_asset');

}
