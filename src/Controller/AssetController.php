<?php

namespace Drupal\bs_asset\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;

/**
 * Returns response for Asset routes.
 */
class AssetController extends ControllerBase {

  public function implementationStatus() {
    $items = [];

    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Setting Up Organizations'), 'organization');
    $items[] = $this->checkEntity(count($items) + 1, $this->t('Set Up an Inventory Organization'), [
      'inventory_organization' => TRUE,
    ]);
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Define Activity Types'), 'activity_type');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Define Activity Causes'), 'activity_cause');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Define Activity Sources'), 'activity_source');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Define Activity Priorities'), 'activity_priority');
    $items[] = $this->checkEntity(count($items) + 1,
      $this->t('Define Asset Criticality'), 'asset_criticality');

    $items[] = [
      '#markup' => $this->t('Step @step <a href=":url">Remove the implementation status block</a>', [
        '@step' => count($items) + 1,
        ':url' => Url::fromRoute('block.admin_display')->toString(),
      ]),
      '#wrapper_attributes' => [
        'class' => [
          'not-finished',
        ],
      ],
    ];

    $output = [
      '#theme' => 'item_list',
      '#wrapper_attributes' => [
        'class' => [
          'implementation-status',
        ],
      ],
      '#items' => $items,
      '#attached' => [
        'library' => [
          'bs_maintenance/implementation_status',
        ],
      ],
    ];

    $tags = $this->entityTypeManager()->getDefinition('organization')
      ->getListCacheTags();
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('item')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('subinventory')
        ->getListCacheTags());
    $tags = Cache::mergeTags($tags,
      $this->entityTypeManager->getDefinition('master_configuration')
        ->getListCacheTags());
    $output['#cache']['tags'] = $tags;

    return $output;
  }

  protected function checkEntity($step, $title, $entity_type_id, array $properties = []) {
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery();
    foreach ($properties as $key => $value) {
      $query->condition($key, $value);
    }
    $count = $query->count()->execute();
    if ($count) {
      $item = t('Step @step <a href=":url">@title</a>', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.' . $entity_type_id . '.collection')->toString(),
        '@title' => $title,
      ]);
    }
    else {
      $text = t('Step @step <a href=":url">@title</a> (Not Finished)', [
        '@step' => $step,
        ':url' => Url::fromRoute('entity.' . $entity_type_id . '.collection')->toString(),
        '@title' => $title,
      ]);
      $item = [
        '#markup' => $text,
        '#wrapper_attributes' => [
          'class' => [
            'not-finished',
          ],
        ],
      ];
    }
    return $item;
  }

}
