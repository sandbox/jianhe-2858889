Enterprise Asset Management

Project page: https://www.drupal.org/sandbox/jianhe/2858889

Requirements
------------
business_core
(https://www.drupal.org/sandbox/jianhe/2846751)
migrate_source_csv
(https://www.drupal.org/project/migrate_source_csv)
state_machine
(https://www.drupal.org/project/state_machine)

Dependencies
------------
Drupal 8.x

Clone command
-------------
git clone --branch 8.x-2.x jianhe@git.drupal.org:sandbox/jianhe/2858889.git \
bs_asset
