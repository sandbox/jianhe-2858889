<?php

namespace Drupal\cbo_activity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\cbo_activity\ActivityCauseInterface;

/**
 * Defines the Activity cause configuration entity.
 *
 * @ConfigEntityType(
 *   id = "activity_cause",
 *   label = @Translation("Activity cause"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_activity\ActivityCauseForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo\CboConfigEntityListBuilder",
 *   },
 *   admin_permission = "administer activity causes",
 *   config_prefix = "cause",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/activity/cause/add",
 *     "edit-form" = "/admin/activity/cause/{activity_cause}/edit",
 *     "delete-form" = "/admin/activity/cause/{activity_cause}/delete",
 *     "collection" = "/admin/activity/cause",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ActivityCause extends ConfigEntityBase implements ActivityCauseInterface {

  /**
   * The machine name of this Activity cause.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Activity cause.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Activity cause.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
