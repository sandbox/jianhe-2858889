<?php

namespace Drupal\cbo_activity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\cbo_activity\ActivitySourceInterface;

/**
 * Defines the Activity source configuration entity.
 *
 * @ConfigEntityType(
 *   id = "activity_source",
 *   label = @Translation("Activity source"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_activity\ActivitySourceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo\CboConfigEntityListBuilder",
 *   },
 *   admin_permission = "administer activity sources",
 *   config_prefix = "source",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/activity/source/add",
 *     "edit-form" = "/admin/activity/source/{activity_source}/edit",
 *     "delete-form" = "/admin/activity/source/{activity_source}/delete",
 *     "collection" = "/admin/activity/source",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ActivitySource extends ConfigEntityBase implements ActivitySourceInterface {

  /**
   * The machine name of this Activity source.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Activity source.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Activity source.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
