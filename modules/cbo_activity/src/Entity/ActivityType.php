<?php

namespace Drupal\cbo_activity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\cbo_activity\ActivityTypeInterface;

/**
 * Defines the Activity type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "activity_type",
 *   label = @Translation("Activity type"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_activity\ActivityTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo\CboConfigEntityListBuilder",
 *   },
 *   admin_permission = "administer activity types",
 *   config_prefix = "type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/activity/type/add",
 *     "edit-form" = "/admin/activity/type/{activity_type}/edit",
 *     "delete-form" = "/admin/activity/type/{activity_type}/delete",
 *     "collection" = "/admin/activity/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class ActivityType extends ConfigEntityBase implements ActivityTypeInterface {

  /**
   * The machine name of this Activity type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Activity type.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Activity type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
