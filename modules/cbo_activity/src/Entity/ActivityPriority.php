<?php

namespace Drupal\cbo_activity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\cbo_activity\ActivityPriorityInterface;

/**
 * Defines the Activity priority configuration entity.
 *
 * @ConfigEntityType(
 *   id = "activity_priority",
 *   label = @Translation("Activity priority"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_activity\ActivityPriorityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo\CboConfigEntityListBuilder",
 *   },
 *   admin_permission = "administer activity priorities",
 *   config_prefix = "priority",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/activity/priority/add",
 *     "edit-form" = "/admin/activity/priority/{activity_priority}/edit",
 *     "delete-form" = "/admin/activity/priority/{activity_priority}/delete",
 *     "collection" = "/admin/activity/priority",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "description",
 *   }
 * )
 */
class ActivityPriority extends ConfigEntityBase implements ActivityPriorityInterface {

  /**
   * The machine name of this Activity priority.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Activity priority.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of this Activity priority.
   *
   * @var int
   */
  protected $weight;

  /**
   * A brief description of this Activity priority.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
