<?php

namespace Drupal\cbo_activity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining a activity priority entity.
 */
interface ActivityPriorityInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
