<?php

namespace Drupal\cbo_activity\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Activity routes.
 */
class ActivityController extends ControllerBase {

  public function addForm() {
    $item = $this->entityTypeManager()->getStorage('item')->create([
      'type' => 'activity',
    ]);

    $form = $this->entityFormBuilder()->getForm($item);

    return $form;
  }

}
