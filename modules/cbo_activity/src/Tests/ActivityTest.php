<?php

namespace Drupal\cbo_activity\Tests;

/**
 * Tests cbo_activity module.
 *
 * @group cbo_activity
 */
class ActivityTest extends ActivityTestBase {

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = array('block', 'views');

  /**
   * A user with permission to administer organization.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser(array(
      'create activity item',
      'edit activity item',
      'access item',
    ));
  }

  /**
   * Test activity list page.
   */
  function testActivity() {
    $this->drupalPlaceBlock('local_actions_block');

    $this->drupalLogin($this->adminUser);

    $this->drupalGet('admin/activity');
    $this->assertResponse(200);
    $this->assertLinkByHref('admin/activity/add');

    $this->clickLink(t('Add activity'));
    $this->assertResponse(200);
  }

}
