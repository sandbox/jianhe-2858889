<?php

namespace Drupal\cbo_activity\Tests;

use Drupal\cbo_inventory\Tests\InventoryTestBase;

/**
 * Provides helper functions for cbo_activity module tests.
 */
abstract class ActivityTestBase extends InventoryTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['cbo_activity'];

}
