<?php

namespace Drupal\work_order\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\work_order\WorkOrderInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the work_order entity class.
 *
 * @ContentEntityType(
 *   id = "work_order",
 *   label = @Translation("Work Order"),
 *   handlers = {
 *     "view_builder" = "Drupal\work_order\WorkOrderViewBuilder",
 *     "access" = "Drupal\work_order\WorkOrderAccessControlHandler",
 *     "views_data" = "Drupal\work_order\WorkOrderViewsData",
 *     "form" = {
 *       "default" = "Drupal\work_order\WorkOrderForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *   },
 *   base_table = "work_order",
 *   entity_keys = {
 *     "id" = "oid",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *   },
 *   admin_permission = "administer work orders",
 *   links = {
 *     "add-form" = "/admin/work_order/add",
 *     "canonical" = "/admin/work_order/{work_order}",
 *     "edit-form" = "/admin/work_order/{work_order}/edit",
 *     "delete-form" = "/admin/work_order/{work_order}/delete",
 *     "collection" = "/admin/work_order",
 *   },
 * )
 */
class WorkOrder extends ContentEntityBase implements WorkOrderInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['parent'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Parent'))
      ->setDescription(t('The parent work order.'))
      ->setSetting('target_type', 'work_order')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['end_to_start'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('End to Start'))
      ->setDescription(t('The work order\'s start date is dependent on which work order\'s complete.'))
      ->setSetting('target_type', 'work_order')
      ->setDisplayOptions('view', [
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 0,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The timestamp that the work_order was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The timestamp that the work_order was last changed.'));

    return $fields;
  }

}
