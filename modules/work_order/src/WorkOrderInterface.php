<?php

namespace Drupal\work_order;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a work_order entity.
 */
interface WorkOrderInterface extends ContentEntityInterface {

}
