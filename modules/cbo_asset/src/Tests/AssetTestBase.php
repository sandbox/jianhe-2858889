<?php

namespace Drupal\cbo_asset\Tests;

use Drupal\cbo_inventory\Tests\InventoryTestBase;

/**
 * Provides helper functions for cbo_asset module tests.
 */
abstract class AssetTestBase extends InventoryTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['cbo_asset'];

}
