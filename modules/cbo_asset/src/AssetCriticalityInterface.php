<?php

namespace Drupal\cbo_asset;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Provides an interface defining a asset criticality entity.
 */
interface AssetCriticalityInterface extends ConfigEntityInterface, EntityDescriptionInterface {

}
