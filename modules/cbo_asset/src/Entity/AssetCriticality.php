<?php

namespace Drupal\cbo_asset\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\cbo_asset\AssetCriticalityInterface;

/**
 * Defines the Asset criticality configuration entity.
 *
 * @ConfigEntityType(
 *   id = "asset_criticality",
 *   label = @Translation("Asset criticality"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_asset\AssetCriticalityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo\CboConfigEntityListBuilder",
 *   },
 *   admin_permission = "administer asset criticalitys",
 *   config_prefix = "criticality",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/asset/criticality/add",
 *     "edit-form" = "/admin/asset/criticality/{asset_criticality}/edit",
 *     "delete-form" = "/admin/asset/criticality/{asset_criticality}/delete",
 *     "collection" = "/admin/asset/criticality",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "description",
 *   }
 * )
 */
class AssetCriticality extends ConfigEntityBase implements AssetCriticalityInterface {

  /**
   * The machine name of this Asset criticality.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Asset criticality.
   *
   * @var string
   */
  protected $label;

  /**
   * The weight of this Activity priority.
   *
   * @var int
   */
  protected $weight;

  /**
   * A brief description of this Asset criticality.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

}
